import logo from './logo.svg';
import './App.css';
import { Link, Routes, Route } from "react-router-dom";
import TodoList from "./components/TodoList";

import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Container from 'react-bootstrap/Container';

function App() {
  return (
      <div className="App" style={{ display: "flex", flexDirection: "column", minHeight: "100vh" }}>
        <Navbar bg="dark" variant="dark">
          <Container>
            <Navbar.Brand>Projet</Navbar.Brand>
            <Nav className="me-auto">
              <Nav.Link as={Link} to="/todolist">Todo List</Nav.Link>
            </Nav>
          </Container>
        </Navbar>

        <div style={{ flex: "1" }}>
          <Routes>
            <Route path="/todolist" element={< TodoList />} />
          </Routes>
        </div>
      </div>
  );
}

export default App;
