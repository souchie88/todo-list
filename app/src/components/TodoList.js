import React, { useState, useEffect } from "react";

function TodoList() {
    // Nouvelle tache à ajouter
    const [task, setTask] = useState("");
    // Liste des taches
    const [tasks, setTasks] = useState(() => {
        // Récupére les taches stockées dans le localStorage
        const storedTasks = JSON.parse(localStorage.getItem("tasks"));
        // Si aucune tache n'est trouvée, retourner un tableau vide
        return storedTasks || [];
    });

    // Met à jour le localStorage chaque fois que la liste des taches change
    useEffect(() => {
        localStorage.setItem("tasks", JSON.stringify(tasks));
    }, [tasks]);

    // Met à jour l'état 'task' avec la nouvelle valeur de texte
    const handleInputChange = (e) => {
        setTask(e.target.value);
    };

    // Soumission du formulaire d'ajout de taches
    const handleSubmit = (e) => {
        e.preventDefault();
        // Vérifie si la nouvelle tache n'est pas vide
        if (task.trim() === "") return;
        // Ajoute la nouvelle tache à la liste des taches
        setTasks([...tasks, task]);
        setTask("");
    };

    // Gestion de la suppression d'une tache
    const handleDelete = (index) => {
        // Filtre les taches pour enlever celle à l'id donné
        const updatedTasks = tasks.filter((_, i) => i !== index);
        // Met à jour la liste des taches
        setTasks(updatedTasks);
    };

    return (
        <div className="container">
            <br /><br />
            <h1 className="display-4 text-center">Liste des tâches</h1><br /><br />
            <form onSubmit={handleSubmit}>
                <div className="input-group mb-3">
                    <input
                        type="text"
                        className="form-control"
                        value={task}
                        onChange={handleInputChange}
                        placeholder="Entrez une tâche"
                    />
                    <button className="btn btn-primary" type="submit">Ajouter une tâche</button>
                </div>
            </form>
            <ul className="list-group">
                {tasks.map((task, index) => (
                    <li key={index} className="list-group-item d-flex justify-content-between align-items-center">
                        {task}
                        <button className="btn btn-danger" onClick={() => handleDelete(index)}>Supprimer</button>
                    </li>
                ))}
            </ul>
        </div>
    );
}

export default TodoList;
